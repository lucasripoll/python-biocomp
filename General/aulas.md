# Programación en Python para Biología Computacional

## Universidad Nacional de Quilmes - Octubre 2019

## Aulas asignadas

| día            | hora     | aula     |
|----------------|----------|----------|
| lunes 7/10     | 9-13 hs  | UFQ (33) |
|                | 13-18 hs | 230      |
| martes 8/10    | 9-13 hs  |  38      |
|                | 13-18 hs | 228      |
| miércoles 9/10 | 9-13 hs  | 121      |
|                | 13-18 hs | 230      |
| jueves 10/10   | 9-13 hs  | UFQ (33) |
|                | 13-18 hs | 230      |
| viernes 10/10  | 9-13 hs  |  38      |
|                | 13-18 hs | 230      |

